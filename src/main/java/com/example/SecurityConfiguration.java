package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by armin on 2/3/17.
 */

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    static Logger log = Logger.getLogger(WebSecurityConfigurerAdapter.class.getName());



    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(inMemoryUserDetailsManager());
//        auth.inMemoryAuthentication()
//                .withUser("fahimeh").password("123456").roles("USER").and()
//                .withUser("armin").password("123456").roles("USER", "ADMIN");
//        auth.userDetailsService(inMemoryUserDetailsManager());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
//                .anonymous().and()
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        http
                .authorizeRequests()
                    .antMatchers( "/user/add" , "/user/logout").permitAll()
                    .antMatchers( "/user/login").hasRole("USER")
                    .antMatchers("/","/books").hasRole("USER")
                    .antMatchers("/hello").hasAuthority("ADMIN")
                    .anyRequest().authenticated()
                .and()
//                .logout()
//                    .permitAll()
////                    .logoutUrl("sda ")
//                    .and()
                .csrf().disable();

    }


    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager(){

        log.info("bean e inMemory");
        final Properties users = new Properties();
        users.put("user","password,ROLE_USER,enabled"); //add whatever other user you need
        return new InMemoryUserDetailsManager(users);

    }


}
