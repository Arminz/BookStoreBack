package com.example;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by armin on 2/11/17.
 */
public interface BookRepository extends CrudRepository<Book,Long> {
    List<Book> findAll();
}
