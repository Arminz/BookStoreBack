package com.example;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by armin on 2/3/17.
 */
@RestController
@RequestMapping("user")
public class UserController {

    static Logger log = Logger.getLogger(UserController.class.getName());

    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;

    @Autowired
    public UserController(InMemoryUserDetailsManager inMemoryUserDetailsManager) {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
    }

    @RequestMapping(value = "exists/{username}", method = RequestMethod.GET)
    public boolean userExists(@PathVariable("username") String username ) {
        return inMemoryUserDetailsManager.userExists(username);
    }

    @RequestMapping(value = "add",method = RequestMethod.POST)
    public User add(@Valid @RequestBody User user , BindingResult bindingResult) throws Exception {

        log.info("add is called");
        if (bindingResult.hasErrors()) {
            log.info("validating error : " + bindingResult.toString());
            return null;
        }
        registerUser(user);
        return user;
    }


    private void registerUser(User user) throws Exception {
        log.info("going to register : " + user.getUsername());
        ArrayList<GrantedAuthority> x = new ArrayList<GrantedAuthority>();
        x.add(new SimpleGrantedAuthority("ROLE_USER"));
        inMemoryUserDetailsManager.createUser(new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                x));
    }
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public boolean logoutPage (HttpServletRequest request, HttpServletResponse response) {
        log.info("/logout is called.");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return true;//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResponse loginUser()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        LoginResponse cr = new LoginResponse();
        cr.setToken(auth.getName());
        log.info("user logged in : " + auth.getPrincipal().toString());
        log.info("token : " + cr.getToken());
        return  cr;
    }


}