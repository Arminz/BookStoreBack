package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Bean
    public CommandLineRunner demo(BookRepository repository)
    {
        return (args) -> {
            for (int i = 0 ; i < 12 ; i ++)
            {

                log.info("going to save book : " + i);
                Book thisBook = new Book("title" + i , "text text text ");
                repository.save(thisBook);
                log.info(String.format("the book with id : %s has been saved.",thisBook.getId()));
            }
        };

    }
}
