package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by armin on 2/11/17.
 */
@RestController
@RequestMapping("book")
public class BookController {

    private static final Logger log = LoggerFactory.getLogger(Application.class);


    @Autowired
    private BookRepository repository;

    @RequestMapping("/")
    public List<ClientBook> getBooks()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        ArrayList<ClientBook> clientBooks = new ArrayList<>();
        for (Book book : repository.findAll())
            clientBooks.add(new ClientBook(book,auth.getName()));
        return clientBooks;
    }
    @RequestMapping(value = "/reserve" , method = RequestMethod.POST)
    public void reserveBook(@NotNull @RequestBody Long bookId) throws Exception {
        log.info("going to reserve : " + bookId);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Book book = repository.findOne(bookId);
        if (book == null)
            throw new Exception("No Book id exception");
        if (!book.isReserved())
            book.setReservedBy(auth.getName());
        else
            throw new Exception("Book is already reserved");
        repository.save(book);
        log.info("the book is reserved now : " + bookId);
    }

    @RequestMapping(value = "/retain" , method = RequestMethod.POST)
    public void retainBook(@NotNull @RequestBody Long bookId) throws Exception {
        log.info("going to retain : " + bookId);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Book book = repository.findOne(bookId);
        if (book == null)
            throw new Exception("No Book id exception");
        if (book.isReserved() && book.getReservedBy().equals(auth.getName()))
            book.setReservedBy(null);
        else
            throw new Exception("you have not this book reserved.");
        repository.save(book);
        log.info("the book has been retained now.");
    }
}
