package com.example;

/**
 * Created by armin on 2/11/17.
 */
public class ClientBook extends Book {
    private boolean isReserved;
    private boolean isReservedByClient;
    
    public ClientBook(Book book,String clientUsername){
        super(book);
        if (book.getReservedBy() != null) {
            isReserved = true;
            isReservedByClient = clientUsername.equals(book.getReservedBy());
        }
        else isReserved = false;
        reservedBy = "PROTECTED";

    }

    public void setReserved(boolean reserved) {
        isReserved = reserved;
    }

    public boolean getIsReserved()
    {
        return isReserved;
    }

    public void setReservedByClient(boolean reservedByClient) {
        isReservedByClient = reservedByClient;
    }

    public boolean getIsReservedByClient()
    {
        return isReservedByClient;
    }
}
