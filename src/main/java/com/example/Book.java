package com.example;

import com.sun.istack.internal.Nullable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by armin on 2/7/17.
 */
@Entity
public class Book {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String text;
    protected String reservedBy;
    protected Book(){}

    public Book(String title,String text)
    {
        this.title = title;
        this.text = text;
    }

    public Book(Book book) {
        this.text = book.getText();
        this.title = book.getTitle();
        this.id = book.getId();
    }

    public void setTitle(String text){this.text = text;    }
    public String getTitle(){return title;}

    public void setText(String text){this.text = text;}
    public String getText(){return text;}

    public void setReservedBy(String reservedBy)
    {
        this.reservedBy = reservedBy;
    }
    public String getReservedBy(){return reservedBy;}
    public void retain()
    {
        setReservedBy(null);
    }

    public boolean isReserved(){return (reservedBy!=null);}

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
